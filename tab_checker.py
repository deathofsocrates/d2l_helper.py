import os
import re
import sys
import os.path
import argparse 

parser = argparse.ArgumentParser(description = 'searches python homework files for tabs')
parser.add_argument("-d", type = int, help = "point amount for tab deduction")
parser.add_argument("-g", help = "add deduction to grade.txt", action = "store_true")
args = parser.parse_args()

if args.g and not args.d:
    print("GRADING REQUIRES A DEDUCTION\n")
    sys.exit(1)

if args.d:
    deduction = args.d

root = os.path.dirname(os.path.realpath(__file__))

def check_int(passed_int):
    try: 
        int(passed_int)
        return True
    except ValueError:
        return False
        
def check_float(passed_int):
    try: 
        float(passed_int)
        return True
    except ValueError:
        return False

for dirpath, _, filenames in os.walk("."):
    for filename in [f for f in filenames if f.endswith(".py") and not f[:5] == 'test_']:
        reldirpath = os.path.relpath(dirpath, root)
        relfilepath = os.path.join(reldirpath, filename)
        if os.path.isdir(reldirpath):
            os.chdir(reldirpath)
            text = open(filename).read()
            if re.findall(r'\t+.*', text):
                print(relfilepath)
                if args.g and args.g:
                    o = open("grade.txt", 'r')
                    last = o.readlines()[-1]
                    o.close()
                    o = open("grade.txt", 'a')
                    if "/" in last.split()[-1] and len(last.split()) > 0:
                        last_split = last.split("/")
                        if (check_int(last_split[0]) or check_float(last_split[0])):
                            score = str(float(last_split[0]) - float(deduction))
                            if float(score) > 0:
                                last_split[0] = str(score)
                            else:
                                last_split[0] = "0"
                            o.write("\n-" + str(deduction) + " TABS IN SUBMISSION\n")
                            o.write("FINAL SCORE: " + "/".join(last_split))
                            o.close()
                    elif "%" in last.split()[-1] and len(last.split()) > 0:
                        last_split = last.split()
                        if (check_int(last_split[-1][:-1]) or check_float(last_split[-1][:-1])):
                            score = float(last_split[-1][:-1]) - float(deduction)
                            if float(score) > 0:
                                last_split[-1] = str(score) + "%"
                            else:
                                last_split[-1] = "0%"
                            o.write("\n-" + str(deduction) + " TABS IN SUBMISSION\n")
                            o.write("\nFINAL SCORE: " + last_split[-1])
                            o.close()
                    else:
                        o.write("\nFILE HAD TABS, BUT UNABLE TO DEDUCT POINTS\n")
                    o.close()
            os.chdir(root)
