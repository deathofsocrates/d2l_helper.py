#** d2l_helper.py**

## Help Option
The help menu can be accessed by using the -h in terminal.

```python d2l_helper.py -h```

## D2L Zip Files
d2l_helper.py requires a zip file from D2L that holds student's submissions from an assignment that was turned on D2l.

1. Download the required zip files from D2l (including the late submissions if needed).
2. Rename zip files (for ease of use).
3. Make sure zip files are in same directory as d2l_helper.py

##Unzip Functionality
d2l_helper.py can unzip the files that the students submitted into a directory that is named after the student.  The root directory is the "unzip directory" parameter from the script call.  In this root directory there will be a folder for each student that contains all their submissions.  The -l flag will name the folders in a last name first format.  The -c flag will take all submissions with the same filename and leave only one file that was the most recent submission.

```python d2l_helper.py <zip file> <unzip directory> -l -c```

## Grading Functionality
d2l_helper.py can autgrade assignments if a grading script is provided.  This requires the the -c is called (since when need each submission to be unique).  The -g and -e flags are also required grading functionality.  The -e flags looks for expected file(s) required by the grading script, and if they are not present, that students name is added to the not graded list.  The -g flag is the grading script itself.  The -d flag is a directory containing all the supplementary files that the grading script requires.

```python d2l_helper.py <zip file> <unzip directory> -l -c -e <ex1> <ex2> < ...> -g <grading-script>```

##Grading Options
Once d2l_helper.py is running you will be presented with several options:

1. ```ACCEPTED CALLS: python python3 ipython >>>``` is the option on how to invoke your grading script. If you need to add something to the list (for example your grading script works in bash) just type in the name of what you want to add to the invocation list, then select it after it is added.

2. ```WOULD YOU LIKE TO INVOKE A LATE PENALTY? (yes/no)``` this lets you chose whether or not there will be a late penalty added to the final grade.txt file

3. ```PLEASE ENTER A NUMERICAL DEDUCTION AMOUNT:``` is the deduction amount (in numerical form) that you can enter in if you chose to make a late deduction.

##Output Files
d2l_helper.py can generate two diferent .txt files.  

1. The first file is the grade.txt file.  This file captures the stdout and stderr of the grading script. If you want to be able to use the late deduction option, then you need to make sure that the last line in your grading scripts output is in one of these forms:

    a.  xxxxxxxxxx xxxxxxxxxxx xxxxxx 100.0/100, where the x's can be anything.  All that is checked is the format of the last word of the last line.

    b.  xxxxxxxxxx xxxxxxxxxxx xxxxxx 68%, where the x's can be anything.  All that is checked is the format of the last word of the last line.
  

2. The second file is NOT_GRADED_<dir>.txt file.  This is the file thats saves the information of the students who had submissions that could not be graded.  There are three categories in the NOT_GRADED_<dir>.txt file:

    a.  CAN NOT UPDATE LATE GRADE:  This is where the students name gets added if a late deduction was unable to be added to their final score.  Some causes of this are the grading scripts output format does not match what is required, or the student's file did not generate a score at all.

    b.  ZOMBIE PROCESS:  This means that the students process exceeded the timing threshold and had to be force killed (default of 10 under zombie_wait variable).  Some causes of this are infinite recursion or stuck on waiting for input.

    c.  MISSING FILE(S):  This means that the student was missing one or more required files needed by the grading script.  Some causes of this are missing files or the files were not named correctly.

# **d2l.sh**
This is the original bash version of the unzip utility.  The script will make the same directory structure as d2l_helper.py, except all naming is done first name first and none of the files are removed or renamed.  This script will copy over extra files, and it will also create a grade.txt file.  The grade.txt file will automatically have -10 for late submissions if the word "late" is found in the destination directory name.

```Usage: ./d2l.sh [-h] -z <zip file> -d <directory> <file> [...]```