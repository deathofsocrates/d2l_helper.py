#!/bin/bash

shopt -s extglob

usage() 
{
    echo "Usage: $0 [-h] -z <zip file> -d <directory> <file> [...]"
    exit 2
}

d_flag=false
z_flag=false

while getopts ":hz:d:" opt; do
  case $opt in
    z)
      zip="$OPTARG"
      z_flag=true
      ;;
    d)
      dest="$OPTARG"
      d_flag=true
      ;;
    h)
      usage
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

if [ $# -gt 4 ]; then
    shift $(( OPTIND - 1 ))
    files="$@"
fi

if [ $# = 0 ]; then
	usage
	
elif [ $z_flag == false ] || [ $d_flag == false ]; then
    usage

elif [ $z_flag ] && [ $d_flag ]; then
    printf "\n%s\n\n" "---------------------------------- UNZIPPING ----------------------------------"
	mkdir -p $dest
	cd $dest
	unzip -q ../$zip -d .
	find . -type f -iname "*.html" -exec rm -f {} \;
	for file in *; do 
		printf "Formatting filename || %s\n" "$file"
		mv "$file" "${file//+([,\-\ ])/_}";
	done
	printf "\n%s\n\n" "------------------------------ MAKING DIRECTORIES ------------------------------"
    for file in *; do 
        mv "$file" "${file:14}"
	done
	for file in *; do
        dir="${file%_*_*_*_*_*_*.*}"
        mkdir -p "$dir"
        mv "$file" "$dir"
	    printf "%70s   ==>   %s\n" "$file" "$dir"
    done
    find . -maxdepth 1 -type d \! -path "." -exec touch {}/grade.txt \;
    LATE="late"
    if [ -n "$2" -a -z "${2/*$LATE*}" ]; then
        find . -name 'grade.txt' -exec bash -c 'echo "-10 LATE SUBMISSION" > {}' \;
    fi
    IFS=' ' read -a array <<< "$files"
    if [ "${#array[@]}" != 0 ]; then
        for val in "${array[@]}"; do
            find . -mindepth 1 -maxdepth 1 -type d \! -path "." | xargs -n 1 cp -f ./../$val
        done
    fi

fi