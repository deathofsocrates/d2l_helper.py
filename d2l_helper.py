#--------------------------------------------------------IMPORTS------------------------------------------------------------------
from subprocess import DEVNULL, TimeoutExpired, call
from argparse import RawTextHelpFormatter              #to format argparse
from datetime import datetime                          #to create datetime objects
import subprocess                                      #to run student files
import operator                                        #to allow object sorting
import argparse                                        #to allow command line options
import zipfile                                         #to handle and extract zip files
import shutil                                          #to copy files 
import sys                                             #to exit with error codes
import os                                              #to navigate directories and get paths

#--------------------------------------------------------ARGPARSE------------------------------------------------------------------

#create parser with description an epilogue 
parser = argparse.ArgumentParser(usage = "d2l_helper.py zip directory [-h] [-l] [-c] [-d] [-g G [G ...]] [-e E [E ...]]", description = "\
---------------------------------------------------------------------------------------\n\
d2l_helper.py unzips zip files from d2l.  it makes a directory structure based on the |\n\
students name and the files they submitted on d2l.  it can produce a directory        |\n\
structure with either a first name order, or a last name first order.  it can also    |\n\
keep only the most recent version of a file if it's submissions have the same name    |\n\
and file extension.  d2l_helper.py also has the capacity to move files into the       |\n\
newly created directory and to run files in the newly created directory.  when test   |\n\
are used in d2l_helper.py grade and log files are created based on the testing script |\n\
stdout and stderr.                                                                    |\n\
---------------------------------------------------------------------------------------\n\
", formatter_class = RawTextHelpFormatter, add_help = False,epilog = "\
--------------------------------------------------------------------------------------|\n\
           AUTHOR: adam stevens CONTACT: deathofsocrates@email.arizona.edu            |\n\
---------------------------------------------------------------------------------------")
  
#add postional arguments
parser.add_argument("zip", type = str, help = "\
absolute path to zip file to unzip                                    |\n\
-----------------------------------------------------------------------")  
parser.add_argument("directory", type = str, help = "\
absolute path of new output directory                                 |\n\
-----------------------------------------------------------------------")

#add optional arguments
parser.add_argument("-h", action = "help", help = "\
show this help message and exit                                       |\n\
-----------------------------------------------------------------------")
parser.add_argument("-l", help = "\
reverse student name: last, first <middle>                            |\n\
-----------------------------------------------------------------------", action="store_true")
parser.add_argument("-c", help = "\
rename and condense duplicate files                                   |\n\
-----------------------------------------------------------------------", action="store_true")
parser.add_argument("-g", type = str, help = "\
test file to grade with along with supplementary required files:      |\n\
                                                                      |\n\
                           <test_file.py>                             |\n\
                                                                      |\n\
THIS OPTION REQUIRES THAT ALL FILES FOLLOWING -g ARE IN THE SAME      |\n\
DIRECTORY AS d2l_helper.py AND REQUIRES THAT THE -c OPTION IS CALLED  |\n\
-----------------------------------------------------------------------")
parser.add_argument("-e", type = str, nargs = '+', help = "\
the expected homework files required for the automated testing        |\n\
                                                                      |\n\
                     <expected1 expected2 ...>                        |\n\
                                                                      |\n\
REQUIRES THAT THE -c OPTION AND -g OPTION ARE CALLED                  |\n\
----------------------------------------------------------------------|")

parser.add_argument("-d", type = str, help = "\
the directory of supplemental files required for the automated        |\n\
testing.                                                              |\n\
                      script location/files                           |\n\
                                                                      |\n\
REQUIRES THAT THE -c OPTION AND -g OPTION ARE CALLED                  |\n\
----------------------------------------------------------------------|")

parser.add_argument("-z", type = int, help = "\
the amount of time a file has for grading.  if the time is exceeded   |\n\
d2l_helper kills the grading script, logs the file, and then moves    |\n\
on to the next file.  default is 10 seconds. if you experience        |\n\
several zombie thread warnings, try increasing the time.              |\n\
                                                                      |\n\
REQUIRES THAT THE -c OPTION AND -g OPTION ARE CALLED                  |\n\
----------------------------------------------------------------------|")
  
#grab arguments  
args = parser.parse_args()
#--------------------------------------------------------END ARGPARSE--------------------------------------------------------------

#grab script location
script_location = os.path.dirname(os.path.realpath(__file__))

#grab positional paths
hw_dir = script_location + "/" + args.directory
zip_path = script_location + "/" + args.zip

#grab expected file list
expected_list = args.e

#setup for NOT_GRADED file
not_graded = {"MISSING FILE(S)":[], "CAN NOT UPDATE LATE GRADE":[], "ZOMBIE PROCESS":[]}
has_error = False

zombie_wait = 5;

master = open("MASTER_GRADE.txt", 'a')

if args.z and (not args.g and not args.c):
    print()
    print("*--------------------- ~d2l_helper~ --------------------*")
    print("|                                                       |")
    print("|  PROCESS TIME LIMIT REQUIRES -g AND -c OPTIONS        |")
    print("|                                                       |")
    print("*-------------------------------------------------------*")
    print()
    sys.exit(1)

elif args.z:
    zombie_wait = args.z

#default accepted calls
accepted_calls = ["python", "python3", "ipython"]

#--------------------------------------------------------CLASSES------------------------------------------------------------------
class HomeworkFile:
    '''
    class -- HomeworkFile
    instance variables -- date - date object of file
                          shortname - name of file minus d2l info
                          fileobject - file object of student submission
    description - this class holds the info of each unzipped file that was
                  in the d2l zip file.  this object makes it easier to sort
                  by date and to move the file into a new directory.
    protection - all instance variables
    '''

    #initialize 
    def __init__(self, name, file_object):
          
        split_list = hyphen_names_middle(name)
          
        self.__date = create_date_object(split_list[3].strip())
        self.__shortname = name.split()[-1].strip()
        self.__fileobject = file_object
     
    #setters for instance variables
    @property
    def date(self):
        return self.__date
        
    @property
    def shortname(self):
        return self.__shortname
        
    @property
    def fileobject(self):
        return self.__fileobject

#--------------------------------------------------------FUNCTIONS------------------------------------------------------------------

def d2l_sanity_checker():

    if args.g:
        if not os.path.isfile(script_location + "/" + args.g):
            print()
            print("*--------------------- ~d2l_helper~ --------------------*")
            print("|                                                       |")
            print("|  THE REQUIRED TESTING FILE DOES NOT EXIST IN THIS     |")
            print("|  DIRECTORY. PLEASE CHECK THIS DIRECTORY FOR THE       |")
            print("|  FILE(S) AND DOUBLE CHECK THE FILE NAME(S).           |")
            print("|                                                       |")
            print("*-------------------------------------------------------*")
            print()
            sys.exit(1)
        
        if not os.path.isfile(script_location + "/" + args.g):
                print()
                print("*--------------------- ~d2l_helper~ --------------------*")
                print("|                                                       |")
                print("|  THE GRADING FILE YOU REQUESTED IS NOT PRESENT IN     |")
                print("|  THIS LOCATION. PLEASE CHECK THIS DIRECTORY FOR THE   |")
                print("|  FILE AND DOUBLE CHECK THE FILE NAME.                 |")
                print("|                                                       |")
                print("*-------------------------------------------------------*")
                print()
                sys.exit(1)
        
            
        if not args.c:
            print()
            print("*--------------------- ~d2l_helper~ --------------------*")
            print("|                                                       |")
            print("|  IN ORDER TO RUN THE AUTOMATED TESTS THE -c CONDENSE  |")
            print("|  OPTION MUST BE CALLED. THIS IS TO ENSURE THAT ONLY   |")
            print("|  ONE COPY OF EACH SUBMITTED FILE EXISTS.              |")
            print("|                                                       |")
            print("*-------------------------------------------------------*")
            print()
            sys.exit(1)
            
        if not args.e:
            print()
            print("*--------------------- ~d2l_helper~ --------------------*")
            print("|                                                       |")
            print("|  IN ORDER TO RUN THE AUTOMATED TESTS THE -e EXPECTED  |")
            print("|  OPTION MUST BE CALLED. THIS IS TO ENSURE THAT THE    |")
            print("|  FILES HAVE THE CORRECT NAME FOR AUTOMATED TESTING.   |")
            print("|                                                       |")
            print("*-------------------------------------------------------*")
            print()
            sys.exit(1)
            
    if not os.path.isfile(zip_path): 
        print()
        print("*--------------------- ~d2l_helper~ --------------------*")
        print("|                                                       |")
        print("|  THE ZIP FILE YOU REQUESTED IS NOT PRESENT IN THIS    |")
        print("|  LOCATION. PLEASE CHECK THIS DIRECTORY FOR THE FILE   |")
        print("|  AND DOUBLE CHECK THE FILE NAME.                      |")
        print("|                                                       |")
        print("*-------------------------------------------------------*")
        print()
        sys.exit(1)
                               
    if not os.path.isdir(hw_dir):
        os.makedirs(hw_dir)
    else:
        print()
        print("*--------------------- ~d2l_helper~ --------------------*")
        print("|                                                       |")
        print("| DESTINATION DIRECTORY EXISTS. IN ORDER TO PROTECT THE |")
        print("| INTEGRITY OF THE FILES PLEASE CHOOSE ANOTHER LOCATION |")
        print("| OR MAKE THE DESTINATION AVAILABLE.                    |")
        print("|                                                       |")
        print("*-------------------------------------------------------*")
        print()
        sys.exit(1)
        
def grader_sanity_checker(name):

    for item in expected_list:
        if not os.path.isfile(item):
            print("*--------------------- ~d2l_helper~ --------------------*")
            print("|                                                       |")
            print("| MISSING REQUIRED FILE ADDING TO NOT_GRADED_<dir>.txt  |")
            print("|                                                       |")
            print("*-------------------------------------------------------*")
            if name not in not_graded["MISSING FILE(S)"]:
                not_graded["MISSING FILE(S)"].append(name)
                global has_error 
                has_error = True
            return False
    return True
            
def grade_format_warning(name):

    print("*--------------------- ~d2l_helper~ --------------------*")
    print("|                                                       |")
    print("| IMPROPER SCORE FORMAT, LATE GRADES WERE NOT UPDATED   |")
    print("| ADDING TO NOT_GRADED_<dir>.txt                         |")
    print("|                                                       |")
    print("*-------------------------------------------------------*")
    if name not in not_graded["CAN NOT UPDATE LATE GRADE"]:
        not_graded["CAN NOT UPDATE LATE GRADE"].append(name)
        global has_error 
        has_error = True
        
def zombie_thread_warning(name):

    print("*--------------------- ~d2l_helper~ --------------------*")
    print("|                                                       |")
    print("| ZOMBIE PROCESS WAS FORCED TO TERMINATE. ADDING NAME   |")
    print("| TO NOT_GRADED_<dir>.txt                               |")
    print("|                                                       |")
    print("*-------------------------------------------------------*")
    if name not in not_graded["ZOMBIE PROCESS"]:
        not_graded["ZOMBIE PROCESS"].append(name)
        global has_error 
        has_error = True
        
def shell_warning():

    print()
    print("*--------------------- ~d2l_helper~ --------------------*")
    print("|                                                       |")
    print("| ENTER IN HOW YOU INVOKE YOUR TEST SCRIPTS. YOUR SHELL |")
    print("| MUST SUPPORT THIS CHOICE.                             |")
    print("|                                                       |")
    print("*-------------------------------------------------------*")
    print()
    
def check_int(passed_int):
    try: 
        int(passed_int)
        return True
    except ValueError:
        return False
        
def check_float(passed_int):
    try: 
        float(passed_int)
        return True
    except ValueError:
        return False

def create_date_object(date_string):
    """
    creates a datetime object for easy sorting
    
    date_string --  date portion of a d2l filename string
    
    returns datetime object
    """
    
    #remove the commna from d2l date string
    date_list = date_string.replace(',', '').split()
    
    #put a leading zero on hour/min if necessary
    if len(date_list[3]) < 4:
        date_list[3] = "0" + date_list[3]
      
    #format date string to match '%b %d %Y %I:%M%p'
    date_list[3] = date_list[3][:2] + ":" + date_list[3][2:]
    hours = date_list[3] + date_list[4]
    date = " ".join(date_list[:3]) + " " + hours
    
    #create datetime object
    date = datetime.strptime(date, '%b %d %Y %I:%M%p')
    
    return date
    
def convert_name(passed_name):
    '''
    takes the name part of the d2l file and converts it to last, first, middle format.
    handles common name cases: first last and first middle last.  reverses all other
    cases.
    
    passed_name --  name portion of a d2l filename string
    
    returns None
    '''

    #split name for operations
    name_list = passed_name.split()
    
    #if reverse is selected
    if args.l:
        #has middle swap
        if len(name_list) == 3:
            name_list[0], name_list[1], name_list[2] = name_list[2], name_list[0], name_list[1]
        #no middle swap
        elif len(name_list) == 2:
            name_list[0], name_list[1] = name_list[1], name_list[0]
        #otherwise just reverse everything
        else:
            name_list.reverse()
            
    return "_".join(name_list)
    
def hyphen_names_middle(name):
    '''
    helper to deal with name cases that have hyphens in the last name. this
    is problematic since d2l uses hyphens as a delimiter for file naming.
    takes second last name and puts it back with the first last name using
    a hyphen
    
    name --  name portion of a d2l filename string
    
    returns list
    '''

    #split on d2l delimiter
    split_list = name.split("-")
     
    #detect extra name
    if len(split_list) == 6:
        #combine last names with a hyphen in the same index
        split_list[2] +=  ("-" + split_list[3])
        #remove extra name
        split_list.pop(3)
        
    return split_list

#----------------------------------------------------------MAIN--------------------------------------------------------------------
def main():

    d2l_sanity_checker()

    hw_dict = {}

    zfile = zipfile.ZipFile(zip_path, "r")
    
    file_path = os.getcwd()

    for item in zfile.infolist():
        if item.filename != "index.html":
            name = convert_name(hyphen_names_middle(item.filename)[2].strip())
            temp_file = HomeworkFile(item.filename, item)
            if name not in hw_dict:
                hw_dict[name] = {temp_file.shortname:[temp_file]}
            else:
                if temp_file.shortname not in hw_dict[name]:
                    hw_dict[name][temp_file.shortname] = [temp_file]
                else:
                    hw_dict[name][temp_file.shortname].append(temp_file)
                
    for key in hw_dict:
        path = hw_dir + "/" + key
        for item in hw_dict[key]:
            hw_dict[key][item].sort(key=operator.attrgetter('date'))
            if args.c:
                hw_dict[key][item].reverse()
                hw_dict[key][item] = [hw_dict[key][item][0]] 
            if not os.path.isdir(path):
                os.makedirs(path)
            os.chdir(path)                
            for elem in hw_dict[key][item]:
                zfile.extract(elem.fileobject)
            if args.c:
                for file in os.listdir(os.getcwd()):
                    os.rename(file, file.split("-")[-1].strip())
      
    # if using automatic grading functionality
    if args.g:
        shell_warning()
        temp = ""
        for item in accepted_calls:
            temp += (item + " ")
        script_call = input("ACCEPTED CALLS: " + temp.strip() + " >>> ")
        in_accpt = (script_call not in accepted_calls)
        while (in_accpt):
            add_bool = True
            while (add_bool):
                add = input(script_call + " NOT AN ACCEPTED CALL, ADD " + script_call + "? (yes/no) ")
                if add == "no":
                    add_bool = False
                elif add == "yes":
                    accepted_calls.append(script_call)
                    temp += script_call
                    add_bool = False
            script_call = input("ACCEPTED CALLS: " + temp.strip() + " >>> ")
            in_accpt = (script_call not in accepted_calls)
        print()
        
        late = ""
        while ((late != "yes") and (late != "no")):
            late = input("WOULD YOU LIKE TO INVOKE A LATE PENALTY? (yes/no) ")
            
        print()   
            
        if late == "yes":
            master.write('-'*75 + "\n")
            master.write("LATE SUBMISSIONS\n" + "\n")
            master.write('-'*75 + "\n")
            deduction = ""
            while (not check_int(deduction) and not check_float(deduction)):
                deduction = input("PLEASE ENTER A NUMERICAL DEDUCTION AMOUNT: ")
                
        print()
        
        test_file = args.g
        
        for index, tup in enumerate(os.walk(hw_dir)):
            force_kill = False
            if index != 0:
                if args.d:
                    file_list = script_location + "/" + args.d
                    for item in os.walk(file_list):
                        shutil.copy(file_path + "/" + test_file, tup[0])
                        for f in item[2]:
                            shutil.copy(file_list + "/" + f, tup[0])
                else:
                    shutil.copy(file_path + "/" + test_file, tup[0])
                os.chdir(tup[0])
                if grader_sanity_checker(os.path.basename(tup[0])):
                    print("GRADING: " + os.getcwd())

                    #check for zombie thread and kill if necessary.  writes to output if 
                    #thread was killed
                    grade = open('grade.txt', 'a')

                    try:
                        rc = call([script_call, test_file], timeout=zombie_wait,
                                  stdin=DEVNULL, stdout=grade, stderr=grade)
                    except TimeoutExpired as error:
                        force_kill = True #XXX do you need it?
                        zombie_thread_warning(os.path.basename(tup[0])) 
                        with open('grade.txt', 'a') as file: # append multiple errors
                            print("{test_file} timed out: {error}".format(**vars()),
                                  "PLEASE CONTACT THE INSTRUCTOR", sep='\n', file=file)  

                    grade.close()
                    
                    #if process was not zombie                   
                    if not force_kill:  
                        o = open("grade.txt", 'r')
                        last = o.readlines()[-1]
                        o.close()
                        o = open("grade.txt", 'a')
                        #if late option was chosen
                        if late == "no":
                            master.write(str(os.getcwd().split('\\')[-1]) + "\n")
                            master.write(last + "\n")
                            master.write('-'*30 + "\n")
                        elif late == "yes":
                            if "/" in last.split()[-1] and len(last.split()) > 0:
                                last_split = last.split("/")
                                if (check_int(last_split[0]) or check_float(last_split[0])):
                                    score = str(float(last_split[0]) - float(deduction))
                                    if float(score) > 0:
                                        last_split[0] = str(score)
                                    else:
                                        last_split[0] = "0"
                                    o.write("-" + deduction + " LATE SUBMISSION\n")
                                    o.write("FINAL SCORE: " + "/".join(last_split))
                                    o.close()
                                else:
                                    grade_format_warning(os.path.basename(tup[0]))
                            elif "%" in last.split()[-1] and len(last.split()) > 0:
                                last_split = last.split()
                                if (check_int(last_split[-1][:-1]) or check_float(last_split[-1][:-1])):
                                    score = float(last_split[-1][:-1]) - float(deduction)
                                    if float(score) > 0:
                                        last_split[-1] = str(score) + "%"
                                    else:
                                        last_split[-1] = "0%"
                                    o.write("-" + deduction + " LATE SUBMISSION\n")
                                    o.write("\nFINAL SCORE: " + last_split[-1])
                                    master.write(str(os.getcwd().split('\\')[-1]) + "\n")
                                    master.write("-" + deduction + " LATE SUBMISSION\n")
                                    master.write("FINAL SCORE: " + last_split[-1] + "\n")
                                    master.write('-'*30 + "\n")
                                    o.close()
                                else:
                                    grade_format_warning(os.path.basename(tup[0]))
                            else:
                                grade_format_warning(os.path.basename(tup[0]))
                            o.close()

        if has_error:
            os.chdir(file_path)
            o = open("NOT_GRADED_" + os.path.basename(hw_dir).upper()  + ".txt", "w")
            for key in not_graded:
                o.write((50 - len(key))//2*"-" + " " + key + " " + (50 - len(key))//2*"-" + "\n")
                for item in not_graded[key]:
                    o.write(item + "\n")
            o.close()

        print()
            
        sys.exit(0)
                    
    sys.exit(0)
                
if __name__ == "__main__":
    main()